<?php
function gen($n){
  $b = array(  'lorem', 'ipsum', 'dolor', 'sit', 'amet', 'consectetur', 'adipiscing', 'elit',
          'a', 'ac', 'accumsan', 'ad', 'aenean', 'aliquam', 'aliquet', 'ante',
          'aptent', 'arcu', 'at', 'auctor', 'augue', 'bibendum', 'blandit',
          'class', 'commodo', 'condimentum', 'congue', 'consequat', 'conubia',
          'convallis', 'cras', 'cubilia', 'curabitur', 'curae', 'cursus',
          'dapibus', 'diam', 'dictum', 'dictumst', 'dignissim', 'dis', 'donec',
          'dui', 'duis', 'efficitur', 'egestas', 'eget', 'eleifend', 'elementum',
          'enim', 'erat', 'eros', 'est', 'et', 'etiam', 'eu', 'euismod', 'ex',
          'facilisi', 'facilisis', 'fames', 'faucibus', 'felis', 'fermentum',
          'feugiat', 'finibus', 'fringilla', 'fusce', 'gravida', 'habitant',
          'habitasse', 'hac', 'hendrerit', 'himenaeos', 'iaculis', 'id',
          'imperdiet', 'in', 'inceptos', 'integer', 'interdum', 'justo',
          'lacinia', 'lacus', 'laoreet', 'lectus', 'leo', 'libero', 'ligula',
          'litora', 'lobortis', 'luctus', 'maecenas', 'magna', 'magnis',
          'malesuada', 'massa', 'mattis', 'mauris', 'maximus', 'metus', 'mi',
          'molestie', 'mollis', 'montes', 'morbi', 'mus', 'nam', 'nascetur',
          'natoque', 'nec', 'neque', 'netus', 'nibh', 'nisi', 'nisl', 'non',
          'nostra', 'nulla', 'nullam', 'nunc', 'odio', 'orci', 'ornare',
          'parturient', 'pellentesque', 'penatibus', 'per', 'pharetra',
          'phasellus', 'placerat', 'platea', 'porta', 'porttitor', 'posuere',
          'potenti', 'praesent', 'pretium', 'primis', 'proin', 'pulvinar',
          'purus', 'quam', 'quis', 'quisque', 'rhoncus', 'ridiculus', 'risus',
          'rutrum', 'sagittis', 'sapien', 'scelerisque', 'sed', 'sem', 'semper',
          'senectus', 'sociosqu', 'sodales', 'sollicitudin', 'suscipit',
          'suspendisse', 'taciti', 'tellus', 'tempor', 'tempus', 'tincidunt',
          'torquent', 'tortor', 'tristique', 'turpis', 'ullamcorper', 'ultrices',
          'ultricies', 'urna', 'ut', 'varius', 'vehicula', 'vel', 'velit',
          'venenatis', 'vestibulum', 'vitae', 'vivamus', 'viverra', 'volutpat',
          'vulputate',
        );     
  $rand = array(); 
  shuffle($b);
  for ($i = 0; $i < $n; $i++) {
    $rand[$i] = $b[$i];
  }
  $rand = implode(" ", $rand);
  return $rand;
}

function db_get_connection() {
  require "config.php";
  static $db;
  try {
    if (!isset($db)) {
      $db = new PDO("mysql:host=localhost;dbname=$dbname", $username, $password);
      $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } 
  } catch(PDOException $ex) {
      echo "Connection failed! " . $ex->getMessage();
  }
  return $db;
}

function add_tables($dbname, $username, $password, $dummyno) {
  try {
    $conn1 = new PDO("mysql:host=$host", $username, $password);
    $conn1->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sql = "DROP DATABASE IF EXISTS " . $dbname . ";";
    $conn1->exec($sql);
    $db = "CREATE DATABASE " . $dbname;
    $usedb = "USE " . $dbname; 
    $conn1->exec($db);
    $conn1->exec($usedb);
    $table1 = "CREATE TABLE blog (
              bid INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
              title TEXT NOT NULL,
              content TEXT NOT NULL,
              date DATE NOT NULL)";    
    $conn1->exec($table1);    
    $table2 = "CREATE TABLE tag (
              tid INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
              tags varchar(100) NOT NULL UNIQUE)";    
    $conn1->exec($table2);
    $table3 = "CREATE TABLE blog_tag_junction (
               blogid INT(11) NOT NULL,
              tagid INT(11) NOT NULL,
              FOREIGN KEY (blogid) REFERENCES blog (bid),
              FOREIGN KEY (tagid) REFERENCES tag (tid))";
    $conn1->exec($table3);      
    $table4 = "CREATE TABLE category  (
              catid INT(10) NOT NULL  AUTO_INCREMENT PRIMARY KEY,
              catname varchar(50) NOT NULL UNIQUE)";
    $conn1->exec($table4);
    $table5 = "CREATE TABLE blog_cat_junction (
              blog_id INT(10) NOT NULL,
              cat_id INT(10) NOT NULL,
              FOREIGN KEY (blog_id) REFERENCES blog(bid),
              FOREIGN KEY (cat_id) REFERENCES category(catid)) ";
    $conn1->exec($table5);
    if (isset($_POST['dummyno'])) {
      for ($k = 0; $k < number_format($dummyno); $k++) {
        $dummycurrentdate = date("Y-m-d H:i:s");
        $dummyt = gen(4);
        $dummyb = gen(100);
        $dummyta = gen(2);
        $dummycat = gen(1);
        $words = explode(" ", $dummyta);
        $s = sizeof($words);  
        for($i = 0; $i < $s; $i++) {
          $words[$i] = trim($words[$i]);
        }
        $dummycat = trim($dummycat);
        $sql = "INSERT INTO blog (title, content, date) VALUES ('$dummyt', '$dummyb', '$dummycurrentdate')";
        $conn1->exec($sql);  
        for ($i = 0; $i < $s; $i++) {
          $resl = $conn1->query("SELECT  tags FROM tag  WHERE  tags = '$words[$i]'");      
          $count = $resl->rowCount();                  
          if ($count==0) {
            $tagsq="INSERT INTO tag (tags)  VALUES('$words[$i]')";
            $conn1->exec($tagsq);
          }
          $relquery = "INSERT INTO blog_tag_junction (blogid, tagid)
                  SELECT bd.bid, tt.tid
                  FROM blog bd JOIN	tag tt
                  ON bd.title = '$dummyt' AND tt.tags = '$words[$i]'";  
          $conn1->exec($relquery);
        }
        $sqlc = "INSERT INTO category (catname) VALUES ('$dummycat') ";
        $conn1->exec($sqlc);

        $cat_realtion = "INSERT INTO blog_cat_junction (blog_id,cat_id)
                        SELECT a.bid , b.catid
                        FROM blog a JOIN category b
                        ON a.title = '$dummyt' AND b.catname = '$dummycat'";
        $conn1->exec($cat_realtion);               
      }
    }
  } catch(PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
  }
}

function add_post($conn, $title, $blogg, $tag, $date,$categoryarray)	{
  $tag = trim($tag, " ");
  $tag = strtolower($tag);
  $words = explode(" ", $tag);
  $s = sizeof($words);
  $s2 = sizeof($categoryarray);
  for ($i = 0; $i < $s; $i++) {
    $words[$i] = trim($words[$i]);
  }
  try { 
    $blogg = addslashes($blogg);
    $sql = "INSERT INTO blog (title, content, date) VALUES ('$title', '$blogg', '$date')";
    $conn->exec($sql);
    for ($i =0; $i < $s; $i++) {
      $resl = $conn->query("SELECT  tags FROM tag  WHERE  tags = '$words[$i]'");
      $count = $resl->rowCount();
      if ($count == 0) {
        $tagsq = "INSERT INTO tag (tags)  VALUES ('$words[$i]')";
        $conn->exec($tagsq);
      }
      $relquery = "INSERT INTO blog_tag_junction (blogid, tagid)
                  SELECT bd.bid, tt.tid
                  FROM blog bd JOIN	tag tt
                  ON bd.title = '$title' AND tt.tags = '$words[$i]'";
      $conn->exec($relquery);

    }
    $getid = $conn->prepare("SELECT bid FROM blog WHERE title = '$title' ");
    $getid->execute();
    $getidval = $getid->fetch();
    $a = $getidval["bid"];
    foreach($categoryarray as $data) {
      $getcat = $conn->prepare("SELECT catid FROM category WHERE catname = '$data' ");
      $getcat->execute();
      $getcatval = $getcat->fetch();
      $b = $getcatval["catid"];
      $insert = "INSERT INTO blog_cat_junction(blog_id,cat_id)
                VALUES ('$a','$b')";
      $conn->exec($insert);     
              
    }
    return TRUE;  
  } catch(PDOException $e) {
      echo "Connection failed: " . $e->getMessage();
  }
}

function update_post($conn, $title, $blogg, $tag, $idval ,$categoryarray) {
  $tag = trim($tag, " ");
	$tag = strtolower($tag);
	$words = explode(" ", $tag);
	$s = sizeof($words);
	for ($i = 0; $i < $s; $i++) {
    $words[$i] = trim($words[$i]);
	}
	try {  
    $blogg = addslashes($blogg);
    $sql = "UPDATE blog SET title = '$title', content = '$blogg' WHERE bid = $idval";
    $conn->exec($sql);
    $del = "DELETE FROM blog_tag_junction where blogid = $idval";
    $conn->exec($del);
    for ($i = 0; $i < $s; $i++) {
    	$resl = $conn->query("SELECT tags FROM tag  WHERE  tags = '$words[$i]'");
    	$count = $resl->rowCount();    
    	if ($count == 0) {
    	  $tagsq = "INSERT INTO tag (tags)  VALUES ('$words[$i]')";
    	  $conn->exec($tagsq);
      }
      $relquery = "INSERT INTO blog_tag_junction (blogid, tagid)
                  SELECT bd.bid, tt.tid
                  FROM blog bd JOIN	tag tt
                  ON bd.bid = $idval AND tt.tags = '$words[$i]'";
      $conn->exec($relquery);
    }
    $delcat = "DELETE FROM blog_cat_junction where blog_id = '$idval'"; 
    $conn->exec($delcat);
    foreach($categoryarray as $data) {
      $getcat = $conn->prepare("SELECT catid FROM category WHERE catname = '$data' ");
      $getcat->execute();
      $getcatval = $getcat->fetch();
      $b = $getcatval["catid"];
      $insert = "INSERT INTO blog_cat_junction(blog_id,cat_id)
                VALUES ('$idval','$b')";
      $conn->exec($insert);     
              
    }
    

  	return TRUE;  
  } catch(PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
  }
}

function post_displayer($conn, $id)	{
$stmt = $conn->prepare("SELECT  title , content, date FROM  blog WHERE bid=?");
$stmt->execute([$id]); 
return 	$stmt->fetch();
}

function all_post_displayer($conn, $offset, $n, $sort)	{
  $stmt = $conn->prepare("SELECT bid, title, content, date FROM blog 
  ORDER BY bid $sort LIMIT $offset, $n");                 
  $stmt->execute();
  $data = $stmt->fetchAll();
  return $data;	
}

function related_post_displayer($conn, $offset, $n, $sort, $tagidvalue2)	{
  $sql = "SELECT bid, title, content, date FROM blog, blog_tag_junction 
        WHERE blog.bid = blog_tag_junction.blogid and $tagidvalue2 = blog_tag_junction.tagid 
        ORDER BY bid $sort LIMIT $offset, $n";                   
  if (filter_var($tagidvalue2, FILTER_VALIDATE_INT)) {
    $stmt = $conn->prepare($sql); 
    $stmt->execute();
    $data = $stmt->fetchAll();
  }
  return $data;
}

function tag_displayer($idval, $conn)	{
  $sql1 = "SELECT tag.tags, tag.tid FROM blog_tag_junction, tag 
          WHERE blog_tag_junction.blogid = ? AND tag.tid = blog_tag_junction.tagid";
  $stmt2 = $conn->prepare($sql1);
  $stmt2->execute([$idval]);
  return $stmt2->fetchAll();
}

function page_counter($n, $conn)	{
  $total_pages_sql = "SELECT bid FROM blog";
  $q1 = $conn->query($total_pages_sql);
  $total_rows = $q1->rowCount();
  $total_pages = ceil($total_rows / $n);
  return $total_pages;
}

function content_trimmer($str)	{
  $words = explode(" ", $str);
  $cont =  implode(" ", array_splice($words, 0, 200));
  if (str_word_count($cont) > 199) {
    $cont= $cont."...";
  }
  return $cont;
}

function post_deleter($conn,$id) {
  $delete_relation = "DELETE FROM  blog_tag_junction WHERE blogid='$id' ";
  $conn->exec($delete_relation);
  $delete_catrel = "DELETE FROM blog_cat_junction WHERE blog_id= '$id' ";
  $conn->exec($delete_catrel);
  $delete_blog= "DELETE FROM blog WHERE bid = '$id' ";
  $conn->exec($delete_blog);
  echo "delete successfull";

}

function category_displayer($val, $conn) {
  $category = "SELECT catname,catid 
               FROM category,blog_cat_junction WHERE
               category.catid = blog_cat_junction.cat_id 
               AND blog_cat_junction.blog_id = '$val'";
  $cstm = $conn->prepare($category); 
  $cstm->execute([$val]);   
  return $cstm->fetchAll();    
}

function allcategory_displayer($conn) {
  $category1 = "SELECT catid,catname FROM category";
  $allcat  = $conn->prepare($category1); 
  $allcat->execute();   
  return $allcat->fetchAll(); 

}

function category_deleter($conn,$cid) {
  try {
  $count = "DELETE FROM category WHERE catid = '$cid'";
  $conn->exec($count);
}
catch(PDOException $e) {
  echo "Category can't be deleted as relation exists";
  //header("location:category.php");
}
}

function category_inserter($conn,$category)  {
  $query = "INSERT INTO category(catname) VALUES ('$categrory')";
  $conn->exec($query);
}

function onecategory_displayer($conn,$catid)  {
  $stmt = $conn->prepare("SELECT catname FROM category WHERE catid = ?");
  $stmt->execute([$catid]); 
  return 	$stmt->fetch();
}

function category_updater($conn,$catid,$category) {
  $query = "UPDATE category SET catname = '$category' WHERE catid ='$catid'";
  $conn->exec($query);

}
?>